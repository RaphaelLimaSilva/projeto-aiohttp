from aiohttp import web
from funcoes import *
from paginas import *

async def romano(request):
    numero = request.match_info.get('numero', 10)
    return web.Response(text=pag_romano(numero),content_type= 'text/html')

async  def cpf(request):
    cpf = request.match_info.get('cpf',0)
    return web.Response(text=pag_cpf(cpf), content_type='text/html')

async def zeros(request):
    numero = request.match_info.get('numero', "1234410000")
    return web.Response(text=pag_zeros(numero), content_type='text/html')


async def senha(request):
    return web.Response(text=pag_senha(), content_type='text/html')


async def default(request):
    return web.Response(text=inicial(), content_type='text/html')

app = web.Application()
app.add_routes([web.get('', default),
                web.get('/romano/{numero}', romano),
                web.get('/valida_cpf/{cpf}',cpf),
                web.get('/dist_zeros/{numero}',zeros),
                web.get('/gera_senha', senha)])

web.run_app(app)