import hashlib
import datetime
import random
import string

def valida_numero(numero):
    try:
        numero = int(numero)
    except:
        return False
    if not (0 < numero < 3999):
        return False
    return True

def numero_para_romano(numero):
    if valida_numero(numero) == True:
        numero = int(numero)
        if numero < 10:
            return (unidade(numero))
        elif numero< 100:
            return (decimal(numero//10)+unidade(numero%10))
        elif numero<1000:
            var = (numero//100) * 100
            return (centena(numero//100)+decimal((numero-var)//10) + unidade((numero-var)%10))
        #tratamento diferente para numeros > 1000, sendo possivel usar para numeros maiores que 4000 caso precise
        else :
            cont = 1
            mil = ""
            while cont <= numero//1000:
                mil = mil+"M"
                cont += 1
            return ( mil+ centena(numero//100%10)+decimal(numero//10%10)+unidade(numero%10))
    else :
        return False

def unidade(uni):
    primeiro = {0: "", 1: "I", 2: "II", 3: "III", 4: "IV", 5: "V", 6: "VI", 7: "VII", 8: "VIII", 9: "IX"}
    return primeiro[uni]

def decimal(dez):
    segundo = {0: "", 1: "X", 2: "XX", 3: "XXX", 4: "XL", 5: "L", 6: "LX", 7: "LXX", 8: "LXXX", 9: "XC"}
    return segundo[dez]

def centena(cen):
    terceiro = {0: "", 1: "C", 2: "CC", 3: "CCC", 4: "CD", 5: "D", 6:"DC", 7: "DCC", 8: "DCCC", 9: "CM"}
    return terceiro[cen]

def retira_formatacao(num_cpf):
    final = ""
    num_cpf = str(num_cpf)
    for x in num_cpf:
        try:
            x = int(x)
            x = str(x)
            final += x
        except:
            x = x
    num_cpf = final
    return num_cpf

def valida_cpf(num_cpf):
    if len(num_cpf) != 11:
        return False

    cpf_invalido_conhecido = num_cpf[0]

    while len(cpf_invalido_conhecido) <= 10:
        cpf_invalido_conhecido = cpf_invalido_conhecido + num_cpf[0]
    if cpf_invalido_conhecido == num_cpf :
        return False
    primeiro = num_cpf[0:9]
    cont = 10
    result = 0
    for x in primeiro:
        x = int(x)
        somar = x * cont
        result = result + somar
        cont = cont - 1
    result = result * 10 % 11
    if result == 10 :
        result = 0
    if int(num_cpf[9]) == result:
        segundo = num_cpf[0:10]
        cont = 11
        result = 0
        for x in segundo:
            x =int(x)
            somar = x * cont
            result = result + somar
            cont = cont -1
        result = result * 10 % 11
        if result == 10 :
            result = 0
        if int(num_cpf[10]) == result:
            return True
    return False

def conta_zeros(string):
    string = string + "1"
    final = 0
    cont = 0
    for indice, letra in enumerate(string):
        if indice < len(string) - 1:
            if string[indice] == "0" and string[indice + 1] == "0":
                cont += 1
            elif string[indice] == "0" and string[indice + 1] != "0":
                cont += 1
                if cont > final:
                    final = cont
                cont = 0


    return final

def gerar_senha():
    letras = random.choices(string.ascii_lowercase, k=10)
    letras = "".join(letras)
    data = datetime.datetime.now()
    data = data.strftime("%H:%M:%S")
    tudo = bytes(data+letras, 'utf-8')
    senha = hashlib.sha256()
    senha.update(tudo)

    return(senha.hexdigest())

def classifica_senhas(pwd):

    nivel_senha = 0
    low = True
    high = True
    dig = True
    pun = True

    if len(pwd) <= 4 :
        return nivel_senha
    elif len(pwd) <=8 :
        nivel_senha += 0
    else :
        nivel_senha += 0.5

    for x in pwd:
        if  low and x in string.ascii_lowercase:
            low = False
            nivel_senha +=0.2
        elif high and  x in string.ascii_uppercase:
            high = False
            nivel_senha += 0.3
        elif dig and x in string.digits:
            dig = False
            nivel_senha += 0.5
        elif pun and x in string.punctuation :
            pun = False
            nivel_senha += 0.5
    return round(nivel_senha)

def hash_md5(pwd)  :
    crip=(pwd.encode())
    return (hashlib.md5(crip)).hexdigest()